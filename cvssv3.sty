% References:
% https://www.first.org/cvss/v3.0/specification-document
% https://www.first.org/cvss/calculator/3.0#<insert vector here>

\usepackage{fp}
\usepackage{tikz}
\usepackage{xcolor}
\usepackage{hyperref}

\newcommand{\FPeq}{TT\fi\FPifeq}
\newcommand{\FPlt}{TT\fi\FPiflt}
\newcommand{\FPgt}{TT\fi\FPifgt}

% calculate CVSSv3 base score
% [AV, AC, PR, UI, S, C, I, A]
\newcommand{\cvssScore}[8]{
  \cvssCalculate{#1}{#2}{#3}{#4}{#5}{#6}{#7}{#8}
  \cvssCalculateResult
}

% generate CVSSv3 vector string
% [AV, AC, PR, UI, S, C, I, A]
\newcommand{\cvssVector}[8]{
  \cvssVectorise{#1}{#2}{#3}{#4}{#5}{#6}{#7}{#8}
  \cvssVectoriseResult
}

% generate colour-coded badge containing CVSSv3 score
% [AV, AC, PR, UI, S, C, I, A]
\newcommand{\cvssBadge}[8]{
  \cvssCalculate{#1}{#2}{#3}{#4}{#5}{#6}{#7}{#8}
  \cvssVectorise{#1}{#2}{#3}{#4}{#5}{#6}{#7}{#8}
  \if\FPeq{\cvssCalculateResult}{0}
    \cvssDrawBadge{green}{NONE}
  \else\if\FPlt{\cvssCalculateResult}{4}
    \cvssDrawBadge{green}{LOW}
  \else\if\FPlt{\cvssCalculateResult}{7}
    \cvssDrawBadge{yellow}{MEDIUM}
  \else\if\FPlt{\cvssCalculateResult}{9}
    \cvssDrawBadge{orange}{HIGH}
  \else
    \cvssDrawBadge{red}{CRITICAL}
  \fi\fi\fi\fi
}

% [internal] draw badge from pre-calculated results
\newcommand{\cvssDrawBadge}[2]{
  \tikz[baseline=(X.base)]
  \node[draw=#1,fill=#1!40,semithick,rectangle,inner sep=2pt, rounded corners=3pt] (X) {
    \href{https://www.first.org/cvss/calculator/3.0\#\cvssVectoriseResult}{
      \color{black}{
        \textbf{#2 (\cvssCalculateResult)}
        \scriptsize{\cvssVectoriseResult}
      }
    }
  };
}

% [internal] generate vector string
\newcommand{\cvssVectorise}[8]{
  \def\cvssVectoriseResult{CVSS:3.0/AV:#1/AC:#2/PR:#3/UI:#4/S:#5/C:#6/I:#7/A:#8}
}

% [internal calculate base score
\newcommand{\cvssCalculate}[8]{
  % parse individual item scores
  \cvssParseAV{#1}
  \cvssParseAC{#2}
  \cvssParsePR{#3}{#5}
  \cvssParseUI{#4}
  \cvssParseCIA{C}{#6}
  \cvssParseCIA{I}{#7}
  \cvssParseCIA{A}{#8}
  % calculate impact sub-score
  \FPeval{cvssImpBase}{1 - ((1 - \cvssC) * (1 - \cvssI) * (1 - \cvssA))}
  \ifnum\pdfstrcmp{#5}{U}=0
    \FPeval{cvssImp}{6.42 * \cvssImpBase}
  \else
    \FPeval{cvssImp}{7.52 * (\cvssImpBase - 0.029) - 3.25 * (\cvssImpBase - 0.02)^15}
  \fi
  % calculate exploitability sub-score
  \FPeval{cvssExp}{8.22 * \cvssAV * \cvssAC * \cvssPR * \cvssUI}
  % caclulate base score
  \if\FPgt{\cvssImp}{0}
    \ifnum\pdfstrcmp{#5}{U}=0
      \FPeval{cvssTemp}{min(\cvssImp + \cvssExp, 10)}
    \else
      \FPeval{cvssTemp}{min(1.08 * (\cvssImp + \cvssExp), 10)}
    \fi
    % return as ceil(round(score, 1))
    \FPeval{ceilTemp}{round(\cvssTemp, 1)}
    \if\FPlt{\ceilTemp}{\cvssTemp}
      \FPeval{ceilTemp}{trunc(\ceilTemp + 0.1, 1)}
    \fi
    \def\cvssCalculateResult{\ceilTemp}
  \else
    % base score is 0 if impact is 0
    \def\cvssCalculateResult{0}
  \fi
}

% [internal] parse attack vector
% [N: network, A: adjacent, L: local, P: physical]
\newcommand{\cvssParseAV}[1]{
  \ifnum\pdfstrcmp{#1}{N}=0
    \def\cvssAV{0.85}
  \else\ifnum\pdfstrcmp{#1}{A}=0
    \def\cvssAV{0.62}
  \else\ifnum\pdfstrcmp{#1}{L}=0
    \def\cvssAV{0.55}
  \else\ifnum\pdfstrcmp{#1}{P}=0
    \def\cvssAV{0.2}
  \else\PackageError{cvss}{bad AV score '#1'}
  \fi\fi\fi\fi
}

% [internal] parse attack complexity
% [L: low, H: high]
\newcommand{\cvssParseAC}[1]{
  \ifnum\pdfstrcmp{#1}{L}=0
    \def\cvssAC{0.77}
  \else\ifnum\pdfstrcmp{#1}{H}=0
    \def\cvssAC{0.44}
  \else\PackageError{cvss}{bad AC score '#1'}
  \fi\fi
}

% [internal] parse privileges required
% [N: none, L: low, H: high]
% [U: unchanged, C: changed]
\newcommand{\cvssParsePR}[2]{
  \ifnum\pdfstrcmp{#2}{U}=0
    \ifnum\pdfstrcmp{#1}{N}=0
      \def\cvssPR{0.85}
    \else\ifnum\pdfstrcmp{#1}{L}=0
      \def\cvssPR{0.62}
    \else\ifnum\pdfstrcmp{#1}{H}=0
      \def\cvssPR{0.27}
    \else\PackageError{cvss}{bad PR score '#1'}
    \fi\fi\fi
  \else\ifnum\pdfstrcmp{#2}{C}=0
    \ifnum\pdfstrcmp{#1}{N}=0
      \def\cvssPR{0.85}
    \else\ifnum\pdfstrcmp{#1}{L}=0
      \def\cvssPR{0.68}
    \else\ifnum\pdfstrcmp{#1}{H}=0
      \def\cvssPR{0.50}
    \else\PackageError{cvss}{bad PR score '#1'}
    \fi\fi\fi
  \else\PackageError{cvss}{bad S score '#2'}
  \fi\fi
}

% [internal] parse user interaction
% [N: none, R: required]
\newcommand{\cvssParseUI}[1]{
  \ifnum\pdfstrcmp{#1}{N}=0
    \def\cvssUI{0.85}
  \else\ifnum\pdfstrcmp{#1}{R}=0
    \def\cvssUI{0.62}
  \else\PackageError{cvss}{bad UI score '#1'}
  \fi\fi
}

% [internal] parse confidentiality/integrity/availability impact
% [H: high, L: low, N: none]
\newcommand{\cvssParseCIA}[2]{
  \ifnum\pdfstrcmp{#2}{H}=0
    \expandafter\def\csname cvss#1\endcsname{0.56}
  \else\ifnum\pdfstrcmp{#2}{L}=0
    \expandafter\def\csname cvss#1\endcsname{0.22}
  \else\ifnum\pdfstrcmp{#2}{N}=0
    \expandafter\def\csname cvss#1\endcsname{0.00}
  \else\PackageError{cvss}{bad #1 score '#2'}
  \fi\fi\fi
}
