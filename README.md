# latex-cvssv3
CVSSv3 calculator for LaTeX.

## Usage
Copy `cvssv3.sty` to your project directory, use as follows:
```latex
\usepackage{cvssv3}

\begin{document}

% calculate base score
\cvssScore{AV}{AC}{PR}{UI}{S}{C}{I}{A}

% generate formatted vector string
\cvssVector{AV}{AC}{PR}{UI}{S}{C}{I}{A}

% generate colour-coded badge containing score and vector
\cvssBadge{AV}{AC}{PR}{UI}{S}{C}{I}{A}

\end{document}
```
